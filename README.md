# README #

#Hioki 3238 driver and device for SILF

The default configuration of Hioki driver (and device):

* internal trigger (:TRIGger:SOURce IMMediate)
* continuous trigger state (:INITiate:CONTinuous 1)
* sampling period set to medium (:SAMPle:RATE MEDium)
* DC voltage measurement ('FUNCtion VOLTage:DC)
* averaging is on (:CALCulate:AVERage:STATe)
* number of averaged measurements is set to 5 (:CALCulate:AVERage 5)

The default configuration can be changed as parameter 'init_com' in:
  - 'start_measurements' method of device
    
  or
  - 'init' method of driver  
  
The 'init_com' parameter is the list of string Hioki commands (see Hioki manual).
For example the default configuration is implemented by:  
```
init_com = [
                ':TRIG:SOUR IMM',
                ':INIT:CONT 1',
                ':SAMP:RATE MED',
                'FUNC VOLT:DC',
                'VOLT:RANG:AUTO 1',
                ':CALC:AVER:STAT 1',
                ':CALC:AVER 5'
        ]
```