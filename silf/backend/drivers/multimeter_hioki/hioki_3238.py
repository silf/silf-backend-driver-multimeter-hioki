from silf.backend.commons.io.text_serial import TextSerial
import sys

import typing

class Hioki3238Driver(TextSerial):
    def __init__(self, port: str, timeout: float=3):
        """
        :param port: Serial port file name (e.g. /dev/ttyUSB0)
        :param timeout: Serial port read timeout:
            None: wait forever
            0: non-blocking mode, return immediately in any case, 
               returning zero or more, up to the requested number of bytes
            x: set timeout to x seconds (float allowed) returns immediately
               when the requested number of bytes are available,
               otherwise wait until the timeout expires and return all bytes that
               were received until then.
        """
        super().__init__(port=port, baud=9600, databits=8, stopbits=1, parity='N', timeout=timeout)
        self.open_port()

    def send_command(self, message: str) -> typing.Optional[str]:
        """
        :param message: command send to Hioki (according to manual) 
        :return: if it is read command (ended with '?') the response to command is returned 
        """
        message = message.strip()
        if message[-1] == '?':
            # if device should response to command
            return super().send_command(message+'\r', 1)[0]
        # non-response command, for example 'FUNC VOLT'
        super().send_command(message+'\r')

    def init(self, init_com: typing.Sequence[str]=None):
        """
        Initialize multimeter according to list of commands in 'init_com'
        (See Hioki manual for list of available commands)
        """
        if not init_com:
            """
            Initialize multimeter:
                - Turn on the internal trigger (:TRIGger:SOURce IMMediate)
                - Turn on the continuous trigger state (:INITiate:CONTinuous 1)
                - Switch the sampling period to medium (:SAMPle:RATE MEDium)
                - DC voltage measurement ('FUNCtion VOLTage:DC)
                - Turn on averaging (:CALCulate:AVERage:STATe)
                - Set the number of averaged measurements to 5 (:CALCulate:AVERage 5)
            """
            init_com = [
                ':TRIG:SOUR IMM',
                ':INIT:CONT 1',
                ':SAMP:RATE MED',
                'FUNC VOLT:DC',
                'VOLT:RANG:AUTO 1',
                ':CALC:AVER:STAT 1',
                ':CALC:AVER 5'
        ]
        for com in init_com:
            self.send_command(com)

    def is_open(self):
        return self.tty and self.tty.isOpen()

    def get(self) -> str:
        """
        :return: measured value from the multimeter
        """
        return self.send_command('FETC?')

    def get_float(self) -> float:
        """
        :return: measured value from the multimeter as float
        """
        return float(self.get())


class MockDriver():
    def open_port(self):
        pass

    def close_port(self):
        pass

    def is_open(self):
        return True

    def init(self, init_com: typing.Sequence[str]=None):
        self.ret =0

    def get_float(self):
        self.ret += 1
        return self.ret

# Test
if __name__ == "__main__":
    print (sys.argv[1])
    hioki = Hioki3238Driver(sys.argv[1], 3)
    hioki.init()
    print (hioki.is_open())
    print (hioki.get_float())
    hioki.close_port()
