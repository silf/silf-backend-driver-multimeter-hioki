from silf.backend.drivers.multimeter_hioki.hioki_3238 import Hioki3238Driver, MockDriver
from silf.backend.commons.util.config import validate_mandatory_config_keys, open_configfile

import typing

class Hioki3238Device():
    device_id = "Hioki"
    live = False

    def init(self, config):
        device_id = self.device_id
        mandatory_config = ['filepath', 'timeout']
        self.config = open_configfile(config)

        if self.config is None:
            raise AssertionError("Config file required")

        print('',self.config, device_id, mandatory_config)
        validate_mandatory_config_keys(self.config, device_id, mandatory_config)

        
        if self.config.getboolean(device_id, 'use_mock', fallback=False):
            self.hioki_driver = MockDriver()
        else:
            self.hioki_driver = Hioki3238Driver(self.config[device_id]['filePath'],
                                                float(self.config[device_id]['timeout']))

    def power_up(self):
        if not self.hioki_driver.is_open():
            self.hioki_driver.open_port()

    def start_measurements(self, init_com: typing.Sequence[str]=None):
        self.hioki_driver.init(init_com)

    def read_state(self) -> float:
        return self.hioki_driver.get_float()

    def power_down(self):
        self.hioki_driver.close_port()