import unittest

from silf.backend.drivers.multimeter_hioki.hioki_3238_device import Hioki3238Device
from silf.backend.commons.util.config import prepend_current_dir_to_config_file

class TestHioki3238Device(unittest.TestCase):
    def test_device_working(self):
        self.device = Hioki3238Device()
        self.device.init(prepend_current_dir_to_config_file("hioki_config.ini"))
        self.device.power_up()
        self.device.start_measurements()
        print(self.device.read_state())
        self.device.power_down()

    def test_mock_working(self):
        self.device = Hioki3238Device()
        self.device.init(prepend_current_dir_to_config_file("mock_config.ini"))
        self.device.power_up()
        self.device.start_measurements()
        print(self.device.read_state())
        self.device.power_down()

if __name__ == '__main__':
    unittest.main()