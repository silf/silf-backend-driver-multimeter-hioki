#!/usr/bin/env python3
# coding=utf-8

from distutils.core import setup
from configparser import ConfigParser
from os import path

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


if __name__ == "__main__":

    cp = ConfigParser()
    file = path.join(path.dirname(__file__), "silf/backend/drivers/multimeter_hioki/version.ini")
    with open(file) as f:
        cp.read_file(f)

    setup(
        name='silf-backend-driver-hioki',
        version=cp['VERSION']['VERSION'],
        packages=['silf.backend.drivers.multimeter_hioki'],
        url='',
        license='',
        author='Silf Team',
        author_email='',
        description='',
    )